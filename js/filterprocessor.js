// extends view since it is slightly more like a view than a model
// edit: No, it should probably be a model!
var FilterProcessor = Backbone.View.extend({
	initialize: function (opts) {

	},
	processFilters: function (filters) {
		var useFuzziness = this.model.get('query').get('fuzziness');
		var fuzzy_params = this.model.get('query').get('fuzzy-params');
		var sortingFields = [];
		// Turn each filter into a query statement
		var filterQueryStatements 
			= filters.map(_.bind(this.processOneFilter,this,sortingFields,useFuzziness,fuzzy_params));
		// Remove empty statements
		filterQueryStatements 
			= _.filter(filterQueryStatements,is);
		// Merge statements into one
		var filterQueryStatement 
			= _.reduce(filterQueryStatements
				,_.bind(this.accumulateStatements,this),'');
		// Update this model with new statements
		this.model.get('query').set({
			filterStatement:filterQueryStatement
			,sortingFields:sortingFields
		});
		this.model.trigger('fetch-needed');
	},
	cleanUpPropName: function (propName) {
		propName=encodeURIComponent(propName);
		return propName;
	},
	processOneFilter: function (sortingFields,useFuzziness,fuzzy_params,simpleFilterModel) {
		assert(this instanceof FilterProcessor, 'Context');
		var id = simpleFilterModel.get('id');

		var filterModel = this.model.get('attributes').get(id);
		// id = this.cleanUpPropName(id);		
		var values = filterModel.get('values');
		if(!values){ // values have not been fetched yet -> no selections
			return;
		}
		
		var queryStatement = '';
		var type = filterModel.get('type');
		if(type=='e'){
			var range = filterModel.get('range');
			var val1 = values.at(range[0]).get('id');
			var val2 = values.at(range[1]).get('id');
			
			queryStatement = id + ":[" + val1 + " TO " + val2 + "]";
			
		} else {
			var selectedValues = values.filter(function (value) {
				return value.get('selected');
			});
			queryStatement 
				= _.reduce(selectedValues,_.bind(this.processOneValue,this,id),'');
			/*if(selectedValues.length >=2){
				sortingFields.push(id);
			}*/
		}
		if (useFuzziness){
			queryStatement += "^"+fuzzy_params[0]
			   +" OR " + id + ":[* TO *]^"+fuzzy_params[1];
		}
		sortingFields.push(id);
		return queryStatement?
				'('+queryStatement+')':'';
	},
	processOneValue: function (attributeName,acc,val) {
		return val.get('selected') // redundant check
				? (acc? acc + ' OR ' : '') 
					+ attributeName +':'+/*encodeURIComponent*/"\""+(val.get('id')+"\"")
				:acc;
	},
	accumulateStatements: function (acc,filterQueryStatement) {
		return filterQueryStatement
			?(acc
				?acc + ' AND ' + filterQueryStatement
				:filterQueryStatement)
			:acc;
	}
});