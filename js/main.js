$(document).ready(function () {
	var urlData = getUrlVars();
	var solrInterface = new SolrInterface2();
	
	var model = new ApplicationModel(solrInterface,urlData);
	
	// Views
	var searchControl = new SearchForm2 ({model:model});
	var facetList = new FacetList2({model:model});
	var filterList = new FilterList2({model:model});
	var resultList = new ResultList2({model:model});
	
});

/* TODO
 * 1. Drag and drop in filterlist X
 * 2a. Use multi valued facet.field to request new values for all filters! X
 * 2b. Highlighted attributes as columns AND Sorting in queries X!
 * 2c. Filter titles are not displayed properly ('p_prop' instead of 'prop') X
 * 2d. Facet names are  not displayed properly ((undefined) etc) X
 * 3. Fix above bug if it still remains after (2a) - Probably not!? (X)
 * 4. 
 * 5. Tools! X
 * 6. Tool: Remove filters, reflect removal in the query (and attribute.selected=false etc..) (see possible issue below too)
 * 7. Remove filters when attribute is no longer present X
 * 8. Tool: Remove all filters /
 * 9. Allow switching value updates on/off with a tool X
 * 10. Allow switching sort by filter on/off with tool. Put the tool below the form element maybe?
 * 11. Restrict drag&drop motion /
 * 12. Display selected filter values alongside the label when not expanded X
 */

/*
 * Possible non tested issue: If an attribute is ruled out by search, (Probably solved)
 * the filter will(?) linger. 
 */