/* TODO
 * Break up parse method (and some clean up here and there)
 */
var SolrInterface2 = Backbone.Model.extend({
	urlRoot: 'http://localhost:8983/solr/qalixaselect/',
	placeQuery : function(query) {
		this.query_data = query.prepare();
		this.fetch();
	},
	
	/*
	 * Custom stringification is a workaround for query_data not being 
	 * able to store multiple instances of the same attribute. This is needed 
	 * in order to request multiple sets of facet values at the same time from Solr.
	 * See: http://wiki.apache.org/solr/SimpleFacetParameters#facet.field
	 */
	stringify: function (data) {
		var l1 = [];
		 _.each(data,function (val,key) {
			if(_.isArray(val)){
				var l2 = _.map(val,function(elem){return encodeURIComponent(key)+'='+encodeURIComponent(elem);});
				l1 = l1.concat(l2);
			} else {
				l1.push( encodeURIComponent(key)+'='+encodeURIComponent(val));
			}
		});
		return l1.join('&');
	},
	sync: function(method, model, options) {
		options.dataType = "jsonp";
		options.jsonp = 'json.wrf';
		options.data = this.stringify(this.query_data);

		options.beforeSend = function(jqXHR, settings) {
			log(settings.url);
		};
		return Backbone.sync(method, model, options);
	},
	parse: function(response) {
		// log(response);
		var property_value_counts_lists = undefined;
		if (response.facet_counts) {
			property_value_counts_lists = {};

			/*
			 * Facet counts: lists the occurrence quantity of
			 * property values of one (or more) properties. The json
			 * representation format is rather weird when it gets
			 * here, we need to parse it to make it useful:
			 */
			_.each(
				_.keys(response.facet_counts.facet_fields),
					function(key) {
								var raw_value_list = response.facet_counts.facet_fields[key];
								var property_value_counts = {};
								var i = 0;
								while (i < raw_value_list.length) {
									var name = raw_value_list[i];
									var freq = raw_value_list[i + 1];
									property_value_counts[name] = freq;
									i = i + 2;
								}
								property_value_counts_lists[key] = property_value_counts;
								assert(_.keys(property_value_counts).length == raw_value_list.length / 2,
										"("+ _.keys(property_value_counts).length + ", "
												+ raw_value_list.length + ")");
								assert(_.keys(property_value_counts_lists).length > 0);
				});
			// alert(js2Str(property_value_lists));
			
		}
		if(property_value_counts_lists){
			return {
				docs : response.response.docs,
				fieldgroups : response.fieldgroups,
				facet_counts : property_value_counts_lists
			};
		} else {
			return {
				docs : response.response.docs,
				fieldgroups : response.fieldgroups
			};
		}
	}
});