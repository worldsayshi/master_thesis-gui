// Make an attribute "public"
function publicAttr(obj, attr, name) {
	obj.__defineGetter__(name, function() {
		return attr;
	});
}

// Remove a property tag from a property name
function clearPropertyTag(str) {
	if (str.substring(0, 2) === "p_") {
		str = str.substring(2);
	}
	return str;
}

function clearElem(elem) {
	// console.log("clearing element: " + id);
	// var elem = document.getElementById(id);
	while (elem.childNodes.length >= 1) {
		elem.removeChild(elem.firstChild);
	}
}

function assert(val, msg) {
	function AssertionError() {
		var self = {
			name : "Assertion Error",
			level : "",
			message : msg,
			htmlMessage : msg
		};
		self.prototype = Error.prototype;
		return self;
	}
	if (!val) {
		log("Assertionerror" + (msg ? ": " + msg : ""));
		throw AssertionError();
	}
}

function log(obj) {
	// console.trace();
	if (obj && obj['myClassName']) {
		console.log("> " + obj['myClassName']);
	} else {
		console.log(obj);
	}
}

/*
 * modified from this:
 * http://papermashup.com/read-url-get-variables-withjavascript/ possibly
 * better:
 * http://stackoverflow.com/questions/1403888/get-url-parameter-with-jquery
 */
function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
			function(m, key, value) {
				vars[key] = decodeURI(value);
			});
	return vars;
}

function pushUrlVar(obj) {
	function serialize(obj) {
		var str = [];
		for ( var fieldName in obj) {
			str.push(encodeURIComponent(fieldName) + "="
					+ encodeURIComponent(obj[fieldName]));
		}
		return str.join('&');
	}
	log(serialize(obj));
	window.history.pushState({}, "", '?' + serialize(obj));
}

function argObjToArray(argsObject) {
	return Array.prototype.slice.call(argsObject);
}

function implements_(interface_, obj) {
	var res = true;
	_.each(Object.keys(interface_), function(functionName) {
		var hasFunction = Boolean(obj[functionName]);
		if (!hasFunction) {
			log('error: object does not implement ' + functionName);
			log(obj[functionName]);
		}
		assert(res, obj + " does not implement " + functionName);
		res = res && obj[functionName];
	});
	assert(res);
}

function isEmpty(o) {
	for ( var p in o) {
		if (o.hasOwnProperty(p)) {
			return false;
		}
	}
	return true;
}

// Like assert, but with warning instead of exception
function check(val, msg) {
	if (!val) {
		log('@ Warning');
		if (msg) {
			log('|-> ' + msg);
		} else {
			// (new Error).trace could give trace as an object
			console.trace();
		}
	}
}

var staticPropNames = [ "name", "articleId", "longDesc", "company", "category",
		"p_e_price", "dyn_field_names" ];
// Doesn't seem like it can be
// retrieved from Solr
function isStaticPropName(name) {
	_.each(staticPropNames, function(pname) {
		if (name === pname) {
			return true;
		}
	});
	return false;
}

function setClassName(obj, name) {
	obj.myClassName = name;
}

/**
 * Transposes a given array.
 * 
 * @id Array.prototype.transpose
 * @author Shamasis Bhattacharya
 * 
 * @type Array
 * @return The Transposed Array
 * @compat=ALL
 * @source http://www.shamasis.net/2010/02/transpose-an-array-in-javascript-and-jquery/
 */
var transposeMatr = function(matr) {

	// Calculate the width and height of the Array
	var a = matr, w = a.length ? a.length : 0, h = a[0] instanceof Array ? a[0].length
			: 0;

	// In case it is a zero matrix, no transpose routine needed.
	if (h === 0 || w === 0) {
		return [];
	}

	/**
	 * @var {Number} i Counter
	 * @var {Number} j Counter
	 * @var {Array} t Transposed data is stored in this array.
	 */
	var i, j, t = [];

	// Loop through every item in the outer array (height)
	for (i = 0; i < h; i++) {

		// Insert a new row (array)
		t[i] = [];

		// Loop through every item per item in outer array (width)
		for (j = 0; j < w; j++) {

			// Save transposed data.
			t[i][j] = a[j][i];
		}
	}

	return t;
};

// Produces a callback that populate the
// destination with elements created with elementMapping
function Mapping(dest, elementMapping, srcpath) {
	var context = this;
	return function(src) {
		_.each(srcpath, function(subpath) {
			src = src[subpath];
		});
		dest.empty();
		_.each(src, function(dataElem) {
			var elem = elementMapping.call(context, dataElem);
			dest.append(elem);
		});
	};
}

function notNull(obj) {
	return obj !== null;
}

function removeNullElementsInGroups (groups) {
	return _.map(groups,function (group) {
		return _.filter(group,notNull);
	});
}

function is(obj) {
	return Boolean(obj);
} 

function js2Str(jsObj) {
	return JSON.stringify(jsObj);
}