var ResultList2 = Backbone.View.extend({
	el:'#search_results',
	initialize: function () {
		this.listenTo(this.model.get('results'),'reset',this.render);
	},
	render: function (docs) {
		var highlightedAttributes = this.model.getHighlightedAttributes();
		this.$el.html(
				$('<table>').append(
						this.renderHeaders(highlightedAttributes)
						,docs.map(_.bind(this.renderOneRow,this))));
	},
	renderHeaders: function (headers) {
		var attrs = this.model.get('attributes');
		return $('<tr>').append(
			_.map(headers,function (header) {
				var attr = attrs.get(header);
				var pres_name = header;
				if(attr){
					pres_name = attr.get('name');
				}
				return $('<th>').text(pres_name);
			})
		);
	},
	renderOneRow: function (docModel) {
		assert(this instanceof ResultList2);
		var highlightedAttributes = this.model.getHighlightedAttributes();
		return $('<tr>').append(_.map(highlightedAttributes,_.bind(this.renderOneColumn,this,docModel)));
	},
	renderOneColumn: function (docModel,attrName) {
		return $('<td>')
			.html($('<a>')
					.attr('href','javascript:void(0)')
					.text(docModel.get(attrName)));
	}
});