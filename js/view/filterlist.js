// The "filter tray"
var FilterList2 = Backbone.View.extend({
	el: '#filter_view',
	children: {},
	initialize: function () {
		var self = this;
		this.listenTo(this.model.get('filters'),'add',this.render);
		this.listenTo(this.model.get('filters'),'remove',this.render);
		this.dragAndDropOptions =  {
			stop : _.bind(self.filterMove,self),
			handle: '.handle',
			distance: 20
		};
	},
	// The filter moved by dragging;
	// adjust model and reload
	filterMove: function (ev,ui) {
		this.model.get('filters').move(ui.item.attr('id'),ui.item.index());
		this.model.triggerReload();
	},
	render: function (_newFilter,filters) {
		this.$el.html($('<ul>')
			.sortable(this.dragAndDropOptions)
			.disableSelection()
			.append(
				filters.map(_.bind(this.renderOneFilter,this))));
	},
	renderOneFilter: function (filter) {
		var that = this;
		var child = new FilterComponent2({
			id:filter.get('id'),
			model:that.model});
		// The filter is only storing the id,
		// the full attribute model needs to be fetched.
		var filterAttrModel 
			= this.model
			.get('attributes')
			.get(filter.get('id'));
		return child.render(filterAttrModel);
	}
});

// View of a single filter
var FilterComponent2 = Backbone.View.extend({
	el:'<li>',
	initialize: function () {
		var attrModel 
			= this.model.get('attributes').get(this.id);
		assert(attrModel instanceof Attribute);
		this.listenTo(attrModel,'change',_.bind(this.render,this));
	},
	titleClicked: function (ev) {
		assert(this instanceof FilterComponent2);
		this.model.get('attributes').get(ev.target.id).useAsFilter();
	},
	valueClicked: function (ev) {
		assert(this instanceof FilterComponent2);
		this.model.selectValue(this.id,ev.target.id);
	},
	render: function (filterAttrModel) {
		assert(filterAttrModel);
		var toolbar = new ToolView({
			model:filterAttrModel.get('toolbar')});
		
		return this.$el.empty()
				.attr('id',filterAttrModel.get('id'))
				.append(
				[toolbar.render()
				 ,$('<a>')
				 	.attr('id',filterAttrModel.get('id'))
				 	.attr('href','javascript:void(0)')
				 	.click('click',_.bind(this.titleClicked,this))
				 	.addClass('handle')
				 	.text(filterAttrModel.get('name'))
				 ,this.renderValueSummary(filterAttrModel)
				 ].concat((filterAttrModel.get('in_focus')
				 			?[this.renderValues(filterAttrModel)]
				 			:[])));
	},
	renderValues: function (filterAttrModel) {
		var valuesCollection = filterAttrModel.get('values');
		if(!valuesCollection) {return;}
		var type = filterAttrModel.get('type');
		var self = this;
		if (type=='ne'){
			return $('<ul>')
				.attr('id','val_list')
				.append(valuesCollection.map(_.bind(this.renderValue,this)));
		}
		else if (type=='e') {
			return $('<div>').slider({
				range:true,
				min:0,
				max:valuesCollection.length-1,
				values:[filterAttrModel.get('range')[0],
				        filterAttrModel.get('range')[1]],
				slide: function (event,ui) {
					self.model.selectRangeSilently(filterAttrModel,ui.values);
				},
				stop:function (event, ui) {
					self.model.triggerReload();
				}
			});
		}
	},
	renderValue: function (valueModel) {
		return $('<li>')
				.attr('id',valueModel.get('id'))
				.click('click',_.bind(this.valueClicked,this))
				.addClass(valueModel.get('selected')?'selected':'')
				.html(
				  $('<a>')
				  	.attr('id',valueModel.get('id'))
					.attr('href','javascript:void(0)')
					.text(valueModel.get('id')));
	},
	// Render selected values after 
	// the filter title
	renderValueSummary: function (filterAttrModel) {
		var valuesCollection = filterAttrModel.get('values');
		var selected_names;
		if(valuesCollection){
			if(filterAttrModel.get('type')=='e'){
				var range = filterAttrModel.get('range');
				var val1 = valuesCollection.at(range[0]).get('id');
				var val2 = valuesCollection.at(range[1]).get('id');
				selected_names = val1 + " to " + val2;
			} else {
				
					selected_names = _.map(valuesCollection.filter(
		           		   function (valModel){return valModel.get('selected');})
		          		   ,function (valModel) {return valModel.get('id');});
				
				selected_names = selected_names.join(', ');
			}
		} else {
			selected_names = '';
		}
		return $('<span>').addClass('collapsedValues').text('    '+selected_names);
	}
});