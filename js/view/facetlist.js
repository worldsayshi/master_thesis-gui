var FacetList2 = Backbone.View.extend({
	events: {
		'click a':'select',
		'mousedown a':function () {
			//log('mousedown a');
		},
		'mouseup a':function () {
			//log('mouseup a');
		},
	},
	el:'#facet_view',
	initialize: function () {
		this.listenTo(this.model.get('groupings'),'reset',this.render);
	},
	select: function (ev) {
		this.model.get('attributes').get(ev.target.id).useAsFilter();
	},
	render: function (groups) {
		var facets = this.model.get('attributes');
		this.$el.attr('class','facet_list').html(
			$('<ul>').append(
					groups.map(_.bind(this.renderOneGroup,this,facets))
				));
	},
	renderOneGroup: function (facets,group) {
		return $('<li>').html(
			$('<ul>').append(
				_.map(group.attributes, function(field) {
					return this.renderOne(facets.get(field));
				}, this)));
	},
	renderOne: function (facetModel){
		return $('<li>').addClass('groupElement')
			.html(
				$('<a>')
				.attr('id',facetModel.get('id'))
				.attr('href','javascript:void(0)')
				.text(facetModel.get('name')));
	}
});