var SearchForm2 = Backbone.View.extend({
	el:'#search_form',
	events: {
		'submit':'submit'
	},
	initialize: function () {
		this.setPhrase(this.model.get('query').get('phrase'));
		this.listenTo(this.model.get('query')
				,'change:phrase'
				,_.bind(this.setPhraseOnChange,this));
		this.initSortingToggle();
	},
	initSortingToggle: function () {
		toolbarModel = this.model.get('global-tools');
   		// render part of the tool
   		this.toolbar = new ToolView({
			model:toolbarModel});
   		this.renderToolBar();
   		this.listenTo(toolbarModel,'change',_.bind(this.renderToolBar,this));
   		
	},
	renderToolBar: function (toolbarModel) {
		$('#global_toolbar').empty().append(this.toolbar.render());
	},
	setPhraseOnChange: function (queryModel,b) {
		this.setPhrase(queryModel.get('phrase'));
	},
	setPhrase: function (phrase) {
		$('#search_field').val(phrase);
	},
	submit: function (ev) {
		var phrase = $('#search_field').val();
		pushUrlVar({search:phrase});
		this.model.get('query').set('phrase',phrase);
		this.model.trigger('fetch-needed',this);
		return false;
	}
});