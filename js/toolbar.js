// "Tools" are the small buttons hovering in some elements
// controlling some aspects of their model. Used for toggling 
// states or removing objects 

// ** Tool view **

var ToolView = Backbone.View.extend({
	el:'<ul>',
	render: function () {
		return this.$el.empty().addClass('toolbar').append(
				this.model.map(_.bind(this.renderOneTool,this)));
	},
	renderOneTool: function (toolModel) {
		assert(toolModel.useTool,'Tool has no use method');
		if(!toolModel.getIcon()){
			return $('<li>')
					.addClass('tool')
					.append(
							$('<button>')
								.button()
								.click(_.bind(this.runTool,this,toolModel))
								.text(toolModel.getLabel()));
		}
		return $('<li>')
				.click(_.bind(toolModel.useTool,toolModel))
				.addClass('tool')
				.html(
					$('<span>')
						.addClass('ui-icon')
						.addClass(toolModel.getIcon()));
	},
	runTool: function (toolModel,ev) {
		ev.preventDefault();
		toolModel.useTool();
	}
});

// ** Tool models **

var ToolsCollection = Backbone.Collection.extend({});

var ToolModel = Backbone.Model.extend({
	getIcon: function () {
		if(_.isArray(this.get('icon'))){
			if(this.get('toggle')){
				return this.get('icon')[1];
			} else {
				return this.get('icon')[0];
			}
		}
		return this.get('icon');
	}
});

// Tool for removing filter
var RemoveFilter = ToolModel.extend({
	initialize: function () {
		this.set('toggle',false);
		this.set('icon','ui-icon-close');
	},
	useTool: function (id) {
		this.get('parentModel').dontUseAsFilter(id);
		this.set('toggle',!this.get('toggle'));
	}
});

// Tool for (un)locking value updates of a filter 
var LockValues = ToolModel.extend({
	initialize: function () {
		var self = this;
		this.set('icon',['ui-icon-locked','ui-icon-unlocked']);
		var parentModel = this.get('parentModel');
		this.listenTo(parentModel,'change:allowUpdateValues',function(){
			self.set('toggle',parentModel.get('allowUpdateValues'));
		});
		this.set('toggle',parentModel.get('allowUpdateValues'));
	},
	useTool: function (id) {
		var parentModel = this.get('parentModel');
		parentModel.set('allowUpdateValues',!parentModel.get('allowUpdateValues'));
	}
});

// Tool for toggling sorting of the result list:
// according to score
// OR 
// according to ascending property value
// (and according to order of filters)
var ToggleGlobalSorting = ToolModel.extend({
	initialize: function () {
	},
	useTool: function (id) {
		assert(this.get('parentModel') instanceof ApplicationModel, this.model);
		
		this.get('parentModel').cycleGlobalSorting();
		this.trigger('change');
	},
	getLabel: function () {
		return this.get('id')+":"+this.get('parentModel').get('query').get('sorting');
	}
});

var ToggleFuzziness = ToolModel.extend({
	initialize: function () {
	},
	useTool: function (id) {
		assert(this.get('parentModel') instanceof ApplicationModel, this.model);
		
		this.get('parentModel').toggleFuzziness();
		this.set('toggle',this.get('parentModel').get('query').get('fuzziness'));
	},
	getLabel: function () {
		return this.get('id')+":"+this.get('parentModel').get('query').get('fuzziness');
	}
});

// TODO
//var HideFacetList = ToolModel.extend({ ...

