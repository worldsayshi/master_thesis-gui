
// A toolbar for controlling global settings
var GlobalTools = function (appmodel) {
	assert(appmodel instanceof ApplicationModel);
	var sortToggle = new ToggleGlobalSorting ({id:'sort',parentModel:appmodel});
	var fuzzyToggle = new ToggleFuzziness ({id:'fuzzy',parentModel:appmodel});
	
	var toolbarModel = new ToolsCollection([
	   sortToggle,fuzzyToggle
	]);
	return toolbarModel;
};