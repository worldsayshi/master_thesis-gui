
// Collection of all attributes and as such keeper of most of the data.
// Other collections not contained here try to merely keep references (id's)
// the these attributes/facets/properties.
var Attributes = Backbone.Collection.extend({
	initialize: function (opts,solrInterface) {
		var self = this;
		solrInterface.on('change:fieldgroups',function (model,groups) {
			groups = removeNullElementsInGroups(groups);
			self.fieldGroupsChanged(groups);
		});
		solrInterface.on('change:facet_counts',function (model,facet_counts) {
		    self.setAttributeValues(facet_counts);
		});
	},
	setAttributeValues: function (new_value_counts) {
		_.each(_.keys(new_value_counts),function (attr_name) {
			if(this.get(attr_name))
			{this.get(attr_name).supplyValues(new_value_counts[attr_name]);}
		},this);
	},
	// - Remove attributes not in new list
	// - Add attributes not in old list
	// - Update smartly those that remain (keep values, overwrite group nr)
	fieldGroupsChanged: function (groups) {
		assert(this instanceof Attributes,"Context");
		var new_facets = {};
		_.each(groups,function (group,i) {
			return _.each(group,function (attr_name,j) {
				
				new_facets[attr_name] = true;
				var typeAndName = this.parseFieldName(attr_name);
				
				var attribute = this.get(attr_name) || new Attribute({id:attr_name,type:typeAndName.type});
				
				if(!this.get(attr_name)){this.add(attribute,{silent:true});}
				
				attribute.set({
						name:typeAndName.pres_name,
						type:typeAndName.type,
						group_nr:i,
						internal_group_nr:j
					});
			},this); 
		},this);
		// Remove if not in new groups
		this.each(function(model) {
			if(!new_facets[model.id]){
				if(model)
				this.trigger('attribute-dont-want-to-be-filter',model.id);
				this.remove(model);
			}
		},this);
		
		// This doesn't work as expected: It sometimes replaces models!
		//this.update(_.values(new_facets),{silent:true});
		// remove this and silent above?
		this.trigger('update',this);
	},
	/* parseFieldName
	 * A bit of a hacky way to pass additional type and unit information about the 
	 * facet from an ad hoc type-inferring script of the Solr configuration;
	 * type info is embedded it in the facet name.
	 * This function extract type info and a label name from the raw field name.
	 * */
	parseFieldName: function (raw_attr_name) {
		var type = 'ne';
		var pres_name = '';
		if (raw_attr_name.substring(0, 2) === "p_") {
			raw_attr_name = raw_attr_name.substring(2);
		}
		if (raw_attr_name.substring(0, 2) === "e_") {
			raw_attr_name = raw_attr_name.substring(2);
			type = 'e';
		}
		// Sometimes there is an additional 'p_' here?
		if (raw_attr_name.substring(0, 2) === "p_") {
			raw_attr_name = raw_attr_name.substring(2);
		}
		if(type=='e'){
			var nameAndUnit = raw_attr_name.split(/__/);
			pres_name = nameAndUnit[0].replace(/_/,' ')
						+' ('+nameAndUnit[1].replace(/_/,' ')+')';
		} else {
			pres_name = raw_attr_name.replace(/_/,' ');
		}
		return {
			type: type,
			pres_name:pres_name
		};
	}
});
 
// Contain most data defining the Attribute/Property/Facet(/Filter)
// Grouping and Filter placement is maintained in separate collections  
var Attribute = Backbone.Model.extend({
	initialize: function () {
		assert(this.get('type'));
		this.set('allowUpdateValues',false);
		this.setUpTools();
		if(this.get('type')=='e'){
			this.set('range',[0,0],{silent:true});
		}
	},
	setUpTools: function() {
		var self = this;
		var lockTool = new LockValues({id:'lockTool',parentModel:self});
		var removeTool = new RemoveFilter({id:'removeTool',parentModel:self});
		var toolbar = new ToolsCollection([
			lockTool,removeTool
		]);
		this.set('toolbar',toolbar);
	},
	useAsFilter: function() {
		this.expand();
		this.collection.trigger('attribute-wants-to-be-filter',this.id);
	},
	dontUseAsFilter: function() {
		this.collection.trigger('attribute-dont-want-to-be-filter',this.id);
	},
	// expand when used as a filter
	expand: function() {
		this.set('in_focus',true);
		this.collection.trigger('focus-change',this.id);
	},
	collapse: function () {
		this.set('in_focus',false);
	},
	// Should currently not be called directly but through the application model
	selectValue: function (name,silent) {
		this.get('values').get(name).select();
		if(!silent){
			this.trigger('change',this);
		}
	},
	selectRangeSilently: function (range) {
		this.set('range',range,{silent:true});
	},
	supplyValues: function (new_counts) {
		if(this.get('values')){
			if(!this.get('allowUpdateValues')){
				return;
			}
			this.get('values').update(new_counts,{parse:true,silent:true});
			this.trigger('change',this);
		} else {
			this.set('values',new Values(new_counts,{parse:true,type:this.get('type')}));
		}
		this.get('values').sort({silent:true});
		//this.assertValuesSorted();
	},
	assertValuesSorted: function () {
		var oldVal = new Value({id:"0"});
		this.get('values').each(function (val) {
			assert(oldVal.get('id')<=val.get('id'));
			oldVal=val;
		},this);
	},
	dropValues: function() {
		this.set('values',undefined);
	},
});

// Value collection held by an attribute
var Values = Backbone.Collection.extend({
	initialize: function (data,options) {
		this.type=options.type;
		if(data){
			this.reset(data,{parse:true});
		}
	},
	parse: function (data) {
		return _.map(data,function (elem,key) {
			return new Value({id:key,count:elem});
		},this);
	},
	comparator: function (a,b) {
		if(this.type=='ne'){
			return a.get('count')>b.get('count');
		} else {
			return parseInt(a.get('id'))-parseInt(b.get('id'));
		}
	},
	toList: function () {
		return this.map(function (elem) {
			if(this.get('type')=='e'){
				return parseInt(elem.get('id'));
			} else {
				return elem.get('id');
			}
		},this);
	}
});

// A single value of a facet
var Value = Backbone.Model.extend({
	// Toggle selection
	select: function () {
		this.set('selected',!this.get('selected'));
	}
});
