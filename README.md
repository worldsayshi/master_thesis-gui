

# TODO
 - It seems that it is possible to store reference 
   to one model in multiple collections. This could be used 
   for 'Filters' and 'Groups' collections instead of saving id's, 
   while 'Attributes' should be the primary collection. 
   More info and possible gotchas:
   https://github.com/documentcloud/backbone/issues/604
   I think a lot of the current event spaghetti could be removed this way..
